// const CompressionWebpackPlugin = require('compression-webpack-plugin'); // gzip 压缩
// const productionGzipExtensions = ['js', 'html', 'css'];
const IS_PROD = process.env.NODE_ENV === 'production';
module.exports = {
    publicPath: IS_PROD ? '/by-vue3-ui/' : '',
    pages: {
        index: {
            entry: 'src/main.js',
            template: `public/index.html`,
            filename: 'index.html',
            chunks: ["chunk-vendors", "chunk-common", "index", 'runtime~index'],
            title: '云墨白-音乐播放器'
        }
    },
    devServer: {
        sockHost: "localhost",
        disableHostCheck: true,
        port: 8080, // 端口号
        host: "0.0.0.0",
        https: false, // https:{type:Boolean}
        open: true, //配置自动启动浏览器
        proxy: {
            "/music": {
                // target: "http://127.0.0.1:80/api/v1", // 需要请求的地址
                target: 'http://upload.likeyunba.com/', // 需要请求的地址
                changeOrigin: true, // 是否跨域
                pathRewrite: {
                    "^/imgs": "" // 替换target中的请求地址，也就是说，在请求的时候，url用'/proxy'代替'http://ip.taobao.com'
                }
            },
        },
    },
    productionSourceMap: !IS_PROD,
    configureWebpack: config => {
        if (IS_PROD) {
            // //gzip压缩
            // config.plugins.push(new CompressionWebpackPlugin({
            //     algorithm: 'gzip',
            //     test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'), //匹配文件名
            //     threshold: 10240, //对超过10k的数据进行压缩
            //     minRatio: 0.8,
            //     deleteOriginalAssets: false //是否删除原文件
            // }));

            config.optimization.minimizer[0].options.terserOptions.compress.warnings = false
            config.optimization.minimizer[0].options.terserOptions.compress.drop_console = true
            config.optimization.minimizer[0].options.terserOptions.compress.drop_debugger = true
            config.optimization.minimizer[0].options.terserOptions.compress.pure_funcs = ['console.log']
        }
    },
}